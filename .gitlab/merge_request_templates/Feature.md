<!-- Use this description template for feature requests. -->

## Feature details

<!-- Briefly describe what this MR is about -->


### API changes

<!-- 
    List API functions that were added / modified / removed / ... 
    in subsections.
    Explicitly note if API was not changed:
    
**No API changes.**

-->

**API version:** `x.x.x` ([documentation](link-to-api-document)).

#### Added (Changed / Removed / ...)
* API call `something`



### Configuration file changes

<!-- Briefly describe added / modified / removed configuration entries 

**No changes in config files.**

-->


## Related issues

<!-- Mention the issue(s) this MR closes or is related to, also add them to
 the title to automatically change their status, when applicable. -->

Resolves #XXX.


## Author's checklist

- [ ] Source branch is `feature/*`.
- [ ] Target branch is `develop` or `release/*` (see `/target_branch` quick action at the end of
      file).
- [ ] Descriptive not automatically generated MR title.
- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Changes are described in changelog(s) (unreleased section).
- [ ] API changes are documented in corresponding documents (if any). 
<!-- 
- [ ] No API changes.
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->
- [ ] Contains isolated set of changes related to the feature.
- [ ] Update board label of going-to-be-closed issues to ~Merging.  
      Using quick action (`/board_move ~Merging`) or from the board  
      *The actual close will happen when the changes are merged into master*.


## Reviewer's checklist

- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Changes are described in changelog(s).
- [ ] Contains isolated set of changes related to the feature.
- [ ] All discussions are resolved.
- [ ] CI build succeed (if configured).
- [ ] CI build warnings are reviewed.
- [ ] API changes are documented in corresponding documents (if any). 
<!-- 
- [ ] No API changes.
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->


## Post-merge actions

- [ ] Consider merging into develop manually if original target is `release` 
      branch.

/label ~Feature
/target_branch develop

