<!-- Use this description template for hotfix branches. -->

## Bug details

<!-- Briefly describe what this MR is about -->


### API changes

<!-- 
    Normally API must not change in hotfixes. 
    If changes are required, describe both API changes and the reason of changing API in hotfix.
 -->
**No API changes.**


### Configuration file changes

<!-- Briefly describe added / modified / removed configuration entries 

**No changes in config files.**

-->


## Related issues

<!-- Mention the issue(s) this MR closes or is related to, also add them to
 the title to automatically change their status, when applicable. -->

Resolves #XXX.


## Version tags

<!-- List tags that should be assigned to the merge commit:

* `product/v0.1.2`

-->


## Author's checklist

- [ ] Descriptive not automatically generated MR title.
- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Changes are described in changelog(s).
- [ ] Contains isolated set of changes related to the hotfix.
- [ ] Target branch is `master` (see `/target_branch` quick action at the end of file).
- [ ] No API changes.
<!-- 
- [ ] API changes are documented in corresponding documents (if any). 
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->
- [ ] Update board label of going-to-be-closed issues to ~Merging.  
      Using quick action (`/board_move ~Merging`) or from the board  
      *The actual close will happen when the changes are merged into master*.


## Tester's checklist

- [ ] Try on several random cases.
- [ ] Check if everything described in changelog(s) works as expected.
- [ ] Run on a testing set of cases and review results. Compare with known 
      good results.
- [ ] Approve MR (only if everything works as expected).


## Reviewer's checklist

- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Contains isolated set of changes related to the hotfix.
- [ ] Changes are described in changelog(s).
- [ ] All discussions are resolved.
- [ ] CI build succeed (if configured).
- [ ] CI build warnings are reviewed.
- [ ] Has correct version in changelogs (unreleased header replaced with 
      date-and-version header).
- [ ] No API changes.
<!-- 
- [ ] API changes are documented in corresponding documents (if any). 
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->


## Post-merge actions

- [ ] Assign tags to merge commit after the MR merged into master.
- [ ] Merge manually into other relevant branches.
- [ ] Update labels of the issue(s) that were automatically closed with 
      the merge.  
      Closed issues should not have ~"To Do" ~Doing or ~Merging labels


/label ~Hotfix
/target_branch master
