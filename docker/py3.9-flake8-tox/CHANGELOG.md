# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.


## [1.2.1] - 2021-04-09
The project is now licensed under the MIT license.

### Changes
* Docker images are rebuilt to include updated packages.

[1.2.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/py3.9-flake8-tox/1.2.0...docker/py3.9-flake8-tox/1.2.1


## [1.2.0] - 2021-01-22
An initial release of the Alpine-based Python 3.9 Docker image for linting and `tox`-ing python projects.

Version number follows version of other images, the set of installed python 
packages is the same as in similar docker images (in this project):
- `flake8`
- `flake8-docstrings`
- `flake8-quotes`
- `pep8-naming`
- `flake8_tuple`
- `flake8-bugbear`
- `darglint`
- `flake8-comprehensions`
- `flake8-gl-codeclimate`
- `tox`