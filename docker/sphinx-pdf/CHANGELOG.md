# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.


## [1.1.2] - 2021-06-03
### Fixed
* Fixed missing dependencies required for PDF generation.  
  *New dependencies were introduced as the result of packages update.*

[1.1.2]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/docker/sphinx-pdf/1.1.1...docker/sphinx-pdf/1.1.2


## [1.1.1] - 2021-04-09
The project is now licensed under the MIT license.

### Changes
* Docker images are rebuilt to include updated packages.

[1.1.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/docker/sphinx-pdf/1.1.0...docker/sphinx-pdf/1.1.1


## [1.1.0] - 2020-02-10
### Changed
* This image is now based on the `sphinx:latest` image from this repository.

### Added
* Added packages required to support markdown format in documentation: `m2r` 
  and `recommonmark` (inherited from the updated base image).

[1.1.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/docker/sphinx-pdf/1.0.0...docker/sphinx-pdf/1.1.0


## 1.0.0 - 2019-11-29
The first release of the Alpine 3.9 -based Python 3 Docker image with a set of 
tools required to build html **and PDF** documentation with Sphinx.  
Includes Read-The-Docs Sphinx theme.

### Configuration
* Alpine 3.9
* Python3
* TinyTex distribution with a set of packages required to build PDF 
  documentation.
* Sphinx
* sphinxcontrib-apidoc
* sphinx-rtd-theme
* m2r 
* recommonmark