# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.


## [1.2.1] - 2021-04-09
The project is now licensed under the MIT license.

### Changes
* Docker images are rebuilt to include updated packages.

[1.2.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/alpine-py3-flake8-tox-mypy/1.2.0...docker/alpine-py3-flake8-tox-mypy/1.2.1


## [1.2.0] - 2021-01-22
### Added
* Added additional `flake8` plugins: `flake8-bugbear`, `darglint`, 
  `flake8-comprehensions`.  
  **New plugins are enabled by default.**
* Added support for codeclimate-comaptible flake8 reports (via 
  `flake8-gl-codeclimate` plugin).
* Docker image now includes `jq`

### Fixed
* Reduced image size by disabling `pip` cache.

[1.2.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/alpine-py3-flake8-tox-mypy/1.1.0...docker/alpine-py3-flake8-tox-mypy/1.2.0


## [1.1.0] - 2019-11-27
### Added
* Docker image now includes `git`.

[1.1.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/alpine-py3-flake8-tox-mypy/1.0.0...docker/alpine-py3-flake8-tox-mypy/1.1.0


## 1.0.0 - 2019-11-15
The first release of the Alpine-based Python 3 Docker image for linting, type-chacking and `tox`-ing python projects.