# Editors note: Keep in sync with the copy in ci-templates/telegram.yml

telegram_notify() {
    telegram_notify_usage() {
        cat <<EOM
Usage: telegram_notify [-e <rows | cols | none>] [-c <chat-id>] [-t <token>] [-w <send-timeout>] [-i <if-condition>] [-b <button-text> <button-link> [-b ...]] [-l <layout>] [-a <append-text>] <text>

Send telegram notification in HTML format with optional buttons (links).

This function relies on the existence of "jq" executable.

COMMAND LINE ARGUMENTS

    -a <text>   Append text <text> as a separate line. A subject to condition
                defined with "-i" switch.

    -b <button-text> <button-link>
                Add a button with the specified text and link. A subject to
                condition defined with "-i" switch.

    -c <chat-id>
                An identifier of Telegram chat to deliver notification to.
                Defaults to TELEGRAM_CHAT_ID environment variable.

    -d          Dry run. Do not perform request. Also increases verbosity level.
                (otherwise the command will not have any visible effect).

    -e <rows|cols|none>
                How extra buttons (not included in the layout) should be handled.
                rows - add each extra button on a separate row.
                cols - add all extra buttons in a single additional row.
                none - ignore all buttons not mentioned in the layout.

    -h          Display this help message and exit.

    -i <condition>
                Start conditional block. All following button definitions (-b)
                and append text defitions (-a) are only added if condition is
                not empty.

                A special case is a condition starting with "!" character. This
                character inverts condition. The following definitions are only
                added if the condition is empty. Use Alternative Value syntax
                for a workaround with values that may start from "!" symbol
                (\${parameter:+word}).

                The condition block ends only when the next conditional block
                starts.

                The main use case is to add certain buttons and text only if
                some environment variable is defined. For example:
                telegram_notify -i "\$CI_MERGE_REQUEST_IID" \\
                -a "<b>MR:</b> <a href=\\"\$CI_PROJECT_URL/-/merge_requests/\$\\
                CI_MERGE_REQUEST_IID\\">!\$CI_MERGE_REQUEST_IID</a>" \\
                -b "MR" "\$CI_PROJECT_URL/-/merge_requests/\$CI_MERGE_REQUEST_IID"

    -l <layout> Defines button layout. Buttons in the layout are separated by
                commas, rows by semicolons. Non existent (not defined) buttons
                are silently ignored.

    -m <mode>   Telegram message parse mode.
                Supported modes are: MarkdownV2, HTML and Markdown.
                See also https://core.telegram.org/bots/api#formatting-options.
                Default is HTML.

    -n          Enable notifications. By default messages are sent without
                notification ("disable_notification": true telegram API
                parameter).

    -p          Enable links web preview. By default web preview is disabled
                ("disable_web_page_preview": true Telegram API parameter).


    -t <token>  Telegram bot token.
                Defaults to TELEGRAM_BOT_TOKEN environment variable.

    -v          Increase verbosity. Verbosity of level >=3 prints sensitive
                data (bot token).

    -w <timeout>
                Send timeout in seconds. A timeout when trying to reach Telegram
                servers.
                Defaults to 10 seconds.


ENVIRONMENT VARIABLES

In addition to the command line options above, the function directly supports
environment variables to populate additional buttons. This support simplifies
function use in CI scripts (helps to avoid necessity to build command line in
shell scripts, that can be quite complex).

The function supports the following environment variables:

- TELEGRAM_BUTTONS_BUTTON_*
    The function reads all environment variables starting with
    "TELEGRAM_BUTTONS_BUTTON_". Each variable is expected to contain a single
    button definition in the "text=url" format.

- TELEGRAM_BUTTONS_EXTRA
    The variable defines multiple buttons in the "text1=url1;text2=url2;..."
    format.

    This variable is deprecated and supported for compatibility only.
    The main reason for this variable been deprecated is that it is impossible
    to defined different buttons in different CI jobs and pass them to the
    notification job via GitLab's dotenv artifacts.

    New code should use pluggable variables mechanism described above.


EOM
        1>&2; return 1;
    }

    telegram_notify_print() {
        if [ "$1" -gt $VERBOSITY ]; then
            return
        fi
        shift
        echo "$*"
    }


    local BOT_TOKEN=$TELEGRAM_BOT_TOKEN
    local CHAT_ID=$TELEGRAM_CHAT_ID
    local SEND_TIMEOUT="${TELEGRAM_SEND_TIMEOUT:-10}"
    local LAYOUT=""
    local SKIP_CONDITIONAL="no"
    local TELEGRAM_PARSE_MODE="HTML"
    local TELEGRAM_DISABLE_NOTIFICATION="true"
    local TELEGRAM_DISABLE_WEB_PAGE_PREVIEW="true"
    local NL="
"
    local VERBOSITY=0
    local DRY_RUN="no"
    local BUTTONS_DEF="{}"
    local ADDTEXT=""
    local EXTRA_BUTTONS=";"

    local OPTIND o
    while getopts ":a:b:c:de:i:l:m:npt:vw:h" o; do
        case "${o}" in
            a)
                APPEND_TEXT=${OPTARG}
                if [ "$SKIP_CONDITIONAL" != "yes" ]; then
                    telegram_notify_print 1 "Append: $APPEND_TEXT"
                    ADDTEXT="${ADDTEXT}${NL}${APPEND_TEXT}"
                else
                    telegram_notify_print 1 "Append \"$APPEND_TEXT\" skipped due to conditional block"
                fi
                ;;
            b)
                BUTTON_TEXT=${OPTARG}
                eval "BUTTON_URL=\${$((OPTIND))}"
                OPTIND=$((OPTIND+1))
                if [ "$SKIP_CONDITIONAL" != "yes" ]; then
                    telegram_notify_print 1 "Button: $BUTTON_TEXT -> $BUTTON_URL"
                    BUTTONS_DEF=$(jq -n --argjson bdef "$BUTTONS_DEF" --arg text "$BUTTON_TEXT" --arg url "$BUTTON_URL" '$bdef + {($text): ($url)}')
                else
                    telegram_notify_print 1 "Button \"$BUTTON_TEXT\" skipped due to conditional block"
                fi
                ;;
            c)
                CHAT_ID=${OPTARG}
                telegram_notify_print 3 "Chat ID: $CHAT_ID"
                ;;
            d)
                # Increase verbosity
                DRY_RUN="yes"
                VERBOSITY=$((VERBOSITY+1))
                telegram_notify_print 1 "Enabled dry-run mode. No request will be made."
                ;;
            e)
                # Extra buttons behavior: "rows", "cols", "none"
                case "${OPTARG}" in
                    none)
                        telegram_notify_print 1 "Skipping extra buttons."
                        EXTRA_BUTTONS=""
                        ;;
                    rows)
                        telegram_notify_print 1 "Extra buttons added as separate rows."
                        EXTRA_BUTTONS=";"
                        ;;
                    cols)
                        telegram_notify_print 1 "Extra buttons added in an additional single row."
                        EXTRA_BUTTONS=","
                        ;;
                    *)
                        telegram_notify_print 1 "ERROR: Unsupported value ${OPTARG} for -e switch. Use one of rows, cols or none."
                        return 1
                        ;;
                esac
                ;;
            i)
                # "!" inverts logic
                if [ "${OPTARG:0:1}" = "!" ]; then
                    if [ -z "${OPTARG:1}" ]; then
                        SKIP_CONDITIONAL="no"
                    else
                        SKIP_CONDITIONAL="yes"
                    fi
                else
                    if [ -z "${OPTARG}" ]; then
                        SKIP_CONDITIONAL="yes"
                    else
                        SKIP_CONDITIONAL="no"
                    fi
                fi
                telegram_notify_print 1 "Conditional block: \"${OPTARG}\" (resolved as \"skip:$SKIP_CONDITIONAL\")"
                ;;
            l)
                LAYOUT=${OPTARG}
                telegram_notify_print 1 "Layout: $LAYOUT"
                ;;
            m)
                TELEGRAM_PARSE_MODE=${OPTARG}
                telegram_notify_print 1 "Telegram parse mode: $TELEGRAM_PARSE_MODE"
                ;;
            n)
                TELEGRAM_DISABLE_NOTIFICATION="false"
                telegram_notify_print 1 "Message notification enabled"
                ;;
            p)
                TELEGRAM_DISABLE_WEB_PAGE_PREVIEW="false"
                telegram_notify_print 1 "Message links preview enabled"
                ;;
            t)
                BOT_TOKEN=${OPTARG}
                telegram_notify_print 3 "Bot token: $BOT_TOKEN"
                ;;
            v)
                # Increase verbosity
                VERBOSITY=$((VERBOSITY+1))
                ;;
            w)
                SEND_TIMEOUT=${OPTARG}
                telegram_notify_print 1 "Send timeout: $SEND_TIMEOUT"
                ;;
            h|*)
                telegram_notify_usage
                return
                ;;
        esac
    done
    shift $((OPTIND-1))

    local URL="https://api.telegram.org/bot${BOT_TOKEN}/sendMessage"

    # Add buttons defined in TELEGRAM_BUTTONS_EXTRA variable (deprecated)
    if [ ! -z "$TELEGRAM_BUTTONS_EXTRA" ]; then
        telegram_notify_print 1 "Adding extra buttons from TELEGRAM_BUTTONS_EXTRA variable (deprecated)."
        BUTTONS_DEF=$(jq -n --argjson bdef "$BUTTONS_DEF" --arg bextra "$TELEGRAM_BUTTONS_EXTRA" '$bdef + ($bextra | split(";") | map(split("=")) | map({(.[0]): .[1]}) | add)')
    fi

    # Add buttons defined in TELEGRAM_BUTTONS_BUTTON_* variables
    BUTTONS_DEF=$(jq -n --argjson bdef "$BUTTONS_DEF" '$bdef + (env | keys | map(select(startswith("TELEGRAM_BUTTONS_BUTTON_"))) | map(env[.] | split("=")) | map({(.[0]): .[1]}) | add)')


    # Handle extra button according to the selected mode (-e)
    if [ ! -z "$EXTRA_BUTTONS" ]; then
        EXTRA_BUTTONS=$(jq -n -r --arg bsep "$EXTRA_BUTTONS" --arg bconf "$LAYOUT" --argjson bdef "$BUTTONS_DEF" '$bconf | split(";") | map(split(",")) | add as $layout | $bdef | keys_unsorted | map(select(. as $item | $layout | index($item) == null)) | join($bsep)')
        # This may result in an empty buttons row, but Telegram handles that.
        LAYOUT="$LAYOUT;$EXTRA_BUTTONS"
    fi

    # Add text argument(s)
    local OLDIFS="$IFS"
    IFS="$NL"
    local TEXT="$*"
    IFS="$OLDIFS"

    if [ ! -z "$ADDTEXT" ]; then
        TEXT="${TEXT}${ADDTEXT}"
    fi

    # Check required arguments

    if [ -z "$TEXT" ]; then
        echo "ERROR: Message text is empty."
        return 1
    fi

    if [ -z "$CHAT_ID" ]; then
        echo "ERROR: Telegram chat ID is required. Use -c switch or define TELEGRAM_CHAT_ID environment variable."
        return 1
    fi

    if [ -z "$BOT_TOKEN" ]; then
        echo "ERROR: Telegram bot token is required. Use -t switch or define TELEGRAM_BOT_TOKEN environment variable."
        return 1
    fi

    # Debug output
    telegram_notify_print 1 "Text:"
    telegram_notify_print 1 "-------✂------"
    telegram_notify_print 1 "$TEXT"
    telegram_notify_print 1 "-------✂------"

    telegram_notify_print 1 "Buttons definition:"
    telegram_notify_print 1 "$BUTTONS_DEF" | jq
    telegram_notify_print 1 "Buttons layout: $LAYOUT"

    # Build JSON request
    local JSON=$(jq -n --arg chat_id "$CHAT_ID" --arg disable_notification "$TELEGRAM_DISABLE_NOTIFICATION" --arg parse_mode "$TELEGRAM_PARSE_MODE" --arg disable_web_page_preview "$TELEGRAM_DISABLE_WEB_PAGE_PREVIEW" --arg text "$TEXT" --argjson bdef "$BUTTONS_DEF" --arg bconf "$LAYOUT" '{$chat_id, $text, $disable_notification, $parse_mode, $disable_web_page_preview, reply_markup: {inline_keyboard: $bconf | split(";") | map(split(",") | map(select($bdef[.]) | {text: ., url: $bdef[.]}))}}')

    telegram_notify_print 1 "Request JSON:"
    telegram_notify_print 1 "$JSON" | jq

    telegram_notify_print 3 curl -s --max-time $SEND_TIMEOUT -d "'$JSON'" -H "'Content-Type: application/json'" -X POST $URL

    if [ "$DRY_RUN" != "yes" ]; then
        curl -s --max-time $SEND_TIMEOUT -d "$JSON" -H "Content-Type: application/json" -X POST $URL
    fi
}