# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.


## [v2.0.1] - 2021-04-09
The project is now licensed under the MIT license.

No other changes.

[v2.0.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/powershell/v2.0.0...gitlab-helpers/powershell/v2.0.1


## [v2.0.0] - 2019-04-26

### Changed
* Renamed `Add-Release` to `Add-ReleaseNotes` to explicitly show that this 
  cmdlet adds release notes to a tag.  
  Starting from 11.7 Gitlab has direct support for releases.

### Added 
* Added `New-Release` cmdlet. This Cmdlet creates Gitlab releases (requires 
  Gitlab 11.7+). Releases have name, description and a set of asset links.

[v2.0.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/powershell/v1.0.0...gitlab-helpers/powershell/v2.0.0


## [v1.0.0] - 2018-12-17

The first release of the powershell Gitlab helpers.